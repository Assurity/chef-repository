name 'jenkinsAssurity'
maintainer 'The Authors'
maintainer_email 'you@example.com'
license 'all_rights'
description 'Installs/Configures jenkinsAssurity'
long_description 'Installs/Configures jenkinsAssurity'
version '0.1.29'

depends 'jenkins', '~> 2.6.0'
