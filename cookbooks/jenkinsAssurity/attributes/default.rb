node.default['home'] = '/var/lib/jenkins'

node.default['log_directory'] = '/var/log/jenkins'

node.default['jenkins']['master']['jvm_options'] = '-Djenkins.install.runSetupWizard=false'
node.default['jenkins']['master']['jvm_options'] = '-Dhudson.diyChunking=false'
