include_recipe 'jenkins::master'
#customization by Mrinal
template "#{node['jenkins']['master']['home']}/config.xml" do
   source   'config.xml.erb'
   mode     '0644'
   owner     node['jenkins']['master']['user']
   group     node['jenkins']['master']['group']
   variables({
    :authentication_flag => 'false'
  })
   notifies :restart, 'service[jenkins]', :immediately
 end
