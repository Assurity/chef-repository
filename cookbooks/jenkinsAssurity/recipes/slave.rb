#Create 'jenkins' user
user 'jenkins' do
  comment 'jenkins user'
  system true
  shell '/bin/bash'
end

# copy Jenkins Master's public key to the authorized_keys folder
authorized_keys_file = '/home/jenkins/.ssh/authorized_keys'
file authorized_keys_file do
  owner 'jenkins'
  group 'jenkins'
  mode '0600'
  content 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBkJoAlLMehH+ulVMEG+FXetQXTIPwOzQjIkdQouXIrXdvPj7V6bd7Eww97x3qbhvhk3uZZszdBghNP30dk2nJcj6YnQmxQ91rKzIa2RVWAiLlB7CNouwYWbwSeE/cjMGbRcY1Vk1hXvZkxKq7G6EBljQ2SZjXT8FDU0uWDrMqq1kThqw8SVuom56b1A+WGQDGq/cCSbWDE6IErISj35OrxvvH2Vclf5tfmo2MPvYHx2efVwv0eibFVpa/T+36Zzqw0zaWIcqB7Wonj02k5hUn24vwK1xa6GevRl0+q8uOTJKfL5UdA+p2HL0L8E9++uiEjEKz0Ml1DS3cDjCYvFLz root@ip-172-30-0-64'
end

# Create the directory /share/executor
directory '/share/executor' do
  owner 'jenkins'
  group 'jenkins'
  mode '0755'
  recursive true
  action :create
end
