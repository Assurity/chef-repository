include_recipe 'jenkinsAssurity::disable_authentication'
include_recipe 'jenkins::master'
include_recipe 'jenkinsAssurity::enable_authentication'

jenkins_plugin 'greenballs'
jenkins_plugin 'credentials'
jenkins_plugin 'git'
jenkins_plugin 'matrix-auth'
jenkins_plugin 'matrix-project'
jenkins_plugin 'google-login'
jenkins_plugin 'ssh-slaves'

# Create private key credentials
jenkins_private_key_credentials 'jenkins' do
  id 'fa3aab48-4edc-446d-b1e2-1d89d86f4458'
  description 'Root user private key'
  private_key "-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAwZCaAJSzHoR/rpVTBBvhV3rUF0yD8Ds0IyJHUKLlyK13bz4+
1em3exMMPe8d6m4b4ZN7mWbM3QYITT99HZNpyXI+mJ0JsUPdaysyGtkVVgIi5Qew
jaLsGFm8EnhP3IzBm0XGNVZNYV72ZMSquxuhAZY0NkmY10/BQ1NLlg6zKqtZE4as
PElbqJuem9QPlhkAxqv3Akm1gxOiBKyEo9+Tq8b7x9lXJX+bX5qNjD72B8dnn1cL
9HomxVaWv0/t+mc6sNM2liHKge1qJ49NpOYVJ9uL8CtcWuhnr0ZdPqvLjkySny+V
HQPqdhy9C/BPfvrohIxCs9DJdQ0t3A4wmLxS8wIDAQABAoIBAE2vuo9nHuitpw0r
s1Df27YffwYG2KFPMaK0PEkZx/2d2WSVZ+S008Yao/wrfyc6oAgOCDpOLwS6ShSu
2YlIQ9ns90CLLnut0C0V/hsWa2DEpLqlNm/yJq2zPi9viCXtfDrUazO2+oGM+nkB
EcDc2ubnIPd03Fn2EYvy31BPgAssXQTJX5+F5yD2vNiXWYaK6P9NL83PAn6GR8Dr
+qNcKDYTC9Ai+/KGjzXXC4TtS7aqPLyQ7oGE/FZrCaPVtVg4HZ4I6lXjhav2yBs8
/RAg/KGqak1zSA0UrcaO0KBMiGMWnnBDDwGdfeQYlKh4ywkaGcqKuXwOwdBMIdGc
q0HBjKkCgYEA7xTW4zfBTjVX2nkfzaC5jOR0AakqGsodoxt2W85cHuTdzL0Dctew
t8LTsO5XJIP5x6GU/bZ59I5TB2oGP70jKITTtEADdQ3GiyY/bCHI4gPxfyDFPqVI
aMDgDLPXh/VODnQKA0nnZXw3UzxVKqhLn7on7lvM00q0KLbq6rW/y4UCgYEAz0Mx
CConL6IFpNxxD86I9502NJnoBI+M46hJgLKko6NJXRsF5I/Pg7s83h2tui0YRWah
pRrXY2F25GYBWGC8uq7R/EyU8O3p4pinskcwbJsP17CyOJtyUk8qnL0d2zVDStgL
OvGZZSnzp5qCdO4mJDftw90RvBH6Pdw0qCxiAhcCgYEA5he3TAxf1QYPnCqQShUS
Hp/LacHFzOySI2UNvwNWs0DumdmbFQ6ye9hcZd6sLW96l/9RZfAVZSEIlY0boIJF
8PwMx2AIM8JjP4464bWOIL9Mz13ZlxX5Hx/pQTBgEPAsBEr3S/z3Xztx/4CEMr6v
sXS2c3cUJJGz269XXTY39QUCgYA4pN1mpNaWzwoJ13FS55hJS/7/ezyM2oqfwWNt
N+WFD2BCgNsobyFAmFB3R9ct9ZsXhIgS7nYFJCLxIuulMrMBtjuPX4KpIex00FGs
iIY0eGBpC1J55AnnctlrgsgBG9prPwY6ssLAgimkGbcjq95isaJakDWDl8ozU9ZU
S+TxOwKBgFduqLIsOjghtHSkN9+f2z/2+iIu65MYW3D7X28H7FV7vtmQmcWx8SU1
hQDRv/0NeEzt0KkCRbIOlx3JN7EDfn+E+Uwdn10QeFN08TO6XUmHAujPCmNHE778
Q+QU+htllJ/+TcGJ04o7lTHTybnku0eaRN3irS5fvxCqkkl6EcEp
-----END RSA PRIVATE KEY-----"
end

# Create a slave launched via SSH
jenkins_ssh_slave 'GenericSlave' do
  description 'Run test suites'
  remote_fs   '/share/executor'
  labels      ['Generic', 'AmazonAMI']

  # SSH specific attributes
  host        '52.64.199.90'
  user        'jenkins'
  credentials 'jenkins'
end
