# jenkinsAssurity

Assurity-specific cookbook which is a wrapper around the community Jenkins cookbook


# How to use

For building a Jenkins Master - Federate the node to the Chef Server and assign the role 'jenkins_master' to it.
knife node run_list set JenkinsMaster "role[jenkins_master]"


For building a Jenkins Slave - Federate the node to the Chef Server and assign the role 'jenkins_slave' to it.
knife node run_list set JenkinsSlave "role[jenkins_slave]"