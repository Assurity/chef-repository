name 'nginxAssurity'
maintainer 'The Authors'
maintainer_email 'you@example.com'
license 'all_rights'
description 'Installs/Configures nginxAssurity'
long_description 'Installs/Configures nginxAssurity'
version '0.1.0'

depends 'nginx', '~> 2.7.6'
