#
# Cookbook Name:: nginxAssurity
# Recipe:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

include_recipe 'nginx'

template "#{node['nginx']['dir']}/conf.d/jenkins-sandbox.conf" do
  source 'jenkins.conf.erb'
  owner  'root'
  group  node['root_group']
  mode   '0644'
  notifies :reload, 'service[nginx]', :delayed
end
